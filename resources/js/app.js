/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import Vue from 'vue'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'
import Axios from 'axios'
import router from './router'

Vue.router = router
Vue.use(VueRouter)
// console.log(router)

const app = new Vue({
    el: '#app',
    router,
});

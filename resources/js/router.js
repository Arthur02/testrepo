import VueRouter from 'vue-router'
// Pages
import ExampleComponent from './components/ExampleComponent'
import Example2Component from './components/Example2Component'
import Example3Component from './components/Example3Component'
// Routes



const routes = [
  {
    path: '/vue/alo',
    name: 'Example',
    component: ExampleComponent,
  },
  {
    path: '/vue/blo',
    name: 'Example2',
    component: Example2Component,
  },
  {
    path: '/vue/glo',
    name: 'Example3',
    component: Example3Component,
  },

]
const router = new VueRouter({
  history: true,
  mode: 'history',
  routes,
})
export default router
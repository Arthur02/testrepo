<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\Ratings;

class TableController extends Controller
{
    public function getStudents(Request $r)
    {
    	$students = Users::get();
    	return json_encode($students);
    }

    public function getRatings(Request $r)
    {	
    	$ratings = Ratings::get();
    	return json_encode($ratings); 
    }
    public function saveTheRating(Request $r)
    {
    	$test = Ratings::where('user_id',$r->user)
    	->where('month',$r->month)
    	->where('day', $r->day)
    	->get();

    	if(count($test) > 0){
    		if($r->rate == 0){
	    		Ratings::where('user_id',$r->user)
	    		->where('month',$r->month)
	    		->where('day', $r->day)
	    		->update([
	    			'rate' => NULL,
	    			'changed' => 1
	    		]);
    		}
    		else{
	    		Ratings::where('user_id',$r->user)
	    		->where('month',$r->month)
	    		->where('day', $r->day)
	    		->update([
	    			'rate' => $r->rate,
	    			'changed' => 1
	    		]);
    		}
    	}
    	else{
    		if($r->rate == 0){
		    	$rate = new Ratings;
		    	$rate->rate = NULL;
		    	$rate->user_id = $r->user;
		    	$rate->month = $r->month;
		    	$rate->day = $r->day;
		    	$rate->save();
		    }
		    else{
		    	$rate = new Ratings;
		    	$rate->rate = $r->rate;
		    	$rate->user_id = $r->user;
		    	$rate->month = $r->month;
		    	$rate->day = $r->day;
		    	$rate->save();
		    }
    	}

    }	

}
